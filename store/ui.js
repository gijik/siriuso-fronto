export default {
  namespaced: true,
  state() {
    return {
      loader: false
    };
  },
  getters: {
    loader(state) {
      return state.loader;
    }
  },
  mutations: {
    toggle_loader(state) {
      if (process.client) {
        state.loader = !state.loader;
      }
    }
  }
};
