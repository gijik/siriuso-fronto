import {HttpLink} from 'apollo-link-http';

export default function (context) {
  const WS_PROTOCOL = process.env.baseURL.indexOf('https') === 0 ? 'wss' : 'ws';
  const WS_ENDPOINT = `${WS_PROTOCOL}://${process.env.baseURL.replace(/(https:\/\/)|(http:\/\/)/g, '')}ws/`;

  return {
    wsEndpoint: WS_ENDPOINT,
    persisting: false,
    websocketsOnly: true,
  }
}